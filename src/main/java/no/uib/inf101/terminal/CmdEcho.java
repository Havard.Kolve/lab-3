package no.uib.inf101.terminal;

public class CmdEcho implements Command{
    @Override
  public String run(String[] args){
    StringBuilder str = new StringBuilder();
    for (String i : args) {
        str.append(i).append(' ');
    }
    return str.toString();
    }

  @Override
  public String getName(){
    return "echo";
  }
}
