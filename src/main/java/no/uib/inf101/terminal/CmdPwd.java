package no.uib.inf101.terminal;

public class CmdPwd implements Command{
    Context context = new Context();


    @Override
    public void setContext(Context context){

    }
    
    @Override
    public String getName(){
        return "pwd";
    }

    @Override
    public String run(String[] args){
        return this.context.getCwd().getAbsolutePath();
    }
}
