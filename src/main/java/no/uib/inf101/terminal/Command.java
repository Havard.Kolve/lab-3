package no.uib.inf101.terminal;

public interface Command {
    String run(String[] args);

    String getName();

    default void setContext(Context context) { /* do nothing */ };  
}
